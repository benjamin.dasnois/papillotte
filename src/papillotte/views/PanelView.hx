package papillotte.views;

import coconut.ui.View;
import tink.pure.List;
import coconut.data.Model;
import papillotte.views.Panel.PanelModel;
import coconut.ui.RenderResult;
import daonna.views.tabPanels.UsersPanel;
import daonna.views.tabPanels.EmployeePanel;
import daonna.views.tabPanels.CreateCRAPanel;
import daonna.views.tabPanels.MyCRAsPanel;

@:build(papillotte.macros.PSSBuilder.build('PanelView.css'))
class PanelView extends View {
    @:attribute var model:PanelViewModel;
    //@:attribute var contents:List<Void->RenderResult>;

    @:state var currentIndex:Int = 0;

    function click(index) {
        currentIndex = index;
    }

    function renderPanel(panelModel:PanelModel) {
        trace('renderPANEL');
        switch (panelModel.type) {
            case Users(e):
                trace("LEN", e.length);
                return hxx('<UsersPanel users=${e} />');
            case Employees:
                return hxx('<EmployeePanel employees=${Main.applicationModel.employees} />');
            case CreateCRA:
                return hxx('<CreateCRAPanel />');
            case MyCRAs:
                return hxx('<MyCRAsPanel />');
        }
    }
    
    function render() {
        trace('rendering');
        var currentModel = model.panels.toArray()[currentIndex];
        trace('COUNT', Main.applicationModel.users.length);
        
        return hxx(
        '<div class=${PanelView.__pssClassName}>
            <div class="ongletContainer">
                <for ${i in 0...model.panels.length}>
                    <let panel = ${model.panels.toArray()[i]}>
                        <let className=${"onglet" + (i == currentIndex ? " selected" : "")}>
                            <div class=$className onclick=${click(i)}>${panel.title}</div>
                        </let>
                    </let>
                </for>
            </div>
            <div>
                ${renderPanel(currentModel)}
            </div>
        </div>');
    }
}

class PanelViewModel implements Model {
    @:observable public var panels:List<PanelModel>;

    public function new() {}
}