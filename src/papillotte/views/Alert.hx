package papillotte.views;

import coconut.ui.Children;
import coconut.data.List;
import coconut.data.Model;
import coconut.ui.View;
import coconut.ui.RenderResult;

@:build(papillotte.macros.PSSBuilder.build('Alert.css'))
class Alert extends View {
    /**
        Set to true to display a X icon at the top right.
    **/
    //@:attribute var closable:Bool = false;

    //@:attribute var alertModel:AlertModel;
    @:attribute var buttons:RenderResult;
    @:attribute var title:String = "";
    @:attribute var children:Children;
    @:attribute var visible:Bool = false;

    function render() {
        if (visible) {
            return hxx('
                <div class=${Alert.__pssClassName + "-layer"}>
                    <div class=${Alert.__pssClassName}>
                        <div class="title-bar">$title</div>
                        <div>
                            <for ${child in children}>
                                $child
                            </for>
                        </div>
                        <div class="button-bar">
                            $buttons
                        </div>
                    </div>
                </div>
            ');
        } else {
            return null;
        }
    }
}

@:build(papillotte.macros.PSSBuilder.build('AlertButton.css'))
class AlertButton extends View {
    @:attribute var children:RenderResult;
    @:attribute var onclick:Void->Void = null;

    function render() {
        return hxx('
        <button onclick=$onclick class=${AlertButton.__pssClassName}>
            $children
        </button>
        ');
    }
}

class AlertButtonModel implements Model {
    @:observable var content:RenderResult;
}

class AlertModel implements Model {
    @:observable var buttons:List<AlertButtonModel> = new List();
    @:observable var title:String = "";
}