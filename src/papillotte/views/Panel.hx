package papillotte.views;

import coconut.ui.View;
import coconut.data.Model;
import coconut.ui.RenderResult;
import coconut.ui.Children;
import daonna.models.UserModel;
import coconut.data.List;

class Panel extends View {
    
    function render() {
        trace('Render panel');
        return hxx('
            <div>
            HUHU
            </div>');
    }
}

class PanelModel implements Model {
    @:observable public var title:String;
    @:observable public var type:PanelType;

    public function new() {
        
    }
}

enum PanelType {
    Users(users:List<UserModel>);
    Employees;
    CreateCRA;
    MyCRAs;
}