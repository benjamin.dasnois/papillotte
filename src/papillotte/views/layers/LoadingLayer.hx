package papillotte.views.layers;

import coconut.ui.View;

@:build(papillotte.macros.PSSBuilder.build('LoadingLayer.css'))
class LoadingLayer extends View {
    @:attribute var count:Int = 0;

    function render() {
        if (count <= 0)
            return null;
        
        return hxx('
        <div class=${LoadingLayer.__pssClassName}>
            Loading...
        </div>
        ');
    }
}