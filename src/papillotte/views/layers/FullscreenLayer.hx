package papillotte.views.layers;

import coconut.ui.View;
import coconut.ui.Children;

@:build(papillotte.macros.PSSBuilder.build('FullscreenLayer.css'))
class FullscreenLayer extends View {
    @:attribute var children:Children;

    function render() {
        return hxx('
        <div class=${FullscreenLayer.__pssClassName}>
        <for ${child in children}>
            $child
        </for>
        </div>
        ');
    }
}