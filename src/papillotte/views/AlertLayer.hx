package papillotte.views;

import coconut.data.Model;
import coconut.ui.View;
import coconut.data.List;
import papillotte.views.Alert.AlertModel;
import coconut.ui.RenderResult;

class AlertLayer extends View {
    @:attribute var alertsModel:AlertsModel;

    public function render() {
        var alert = this.alertsModel.alertsList.first();
        switch (alert) {
            case Some(alert):
                return hxx('<div class="plop">$alert</div>');
            case None:
        }
        return hxx('<div class="plopVide"></div>');
    }
}

class AlertsModel implements Model {
    @:observable public var alertsList:List<RenderResult>;
}