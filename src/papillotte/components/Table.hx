package papillotte.components;

import coconut.ui.RenderResult;
import coconut.data.Model;
import coconut.ui.View;
import coconut.data.List;

@:build(papillotte.macros.PSSBuilder.build("Table.css"))
class Table<T> extends View {
    @:attribute var headers:List<String>;
    @:attribute var data:List<T>;
    @:attribute var renderRow:T->RenderResult;

    public function render() {
        return hxx('
        <table class=${Table.__pssClassName}>
            <thead>
                <tr>
                    <for ${header in headers}>
                        <th>$header</th>
                    </for>
                </tr>
            </thead>
            <tbody>
                <for ${datum in data}>
                    ${renderRow(datum)}
                </for>
            </tbody>
        </table>');
    }
}